import Counter from "./Counter";
import { useState } from "react";
let App = () => {
  const [num, setNum] = useState(0);
  const [counter, setCounter] = useState([
    {
      id: 0,
      value: 0,
    },
    {
      id: 1,
      value: 0,
    },
    {
      id: 2,
      value: 0,
    },
    {
      id: 3,
      value: 0,
    },
  ]);
  const cartCountInc = () => {
    if (num === 4) {
      return num;
    }
    setNum((num) => num + 1);
  };
  const cartCountDec = () => {
    if (num === 0) {
      return num;
    }
    setNum((num) => num - 1);
  };
  let refreshCount = () => {
    let updatedState = counter.map((counterObj) => {
      counterObj.value = 0;
      return counterObj;
    });
    setCounter(updatedState);
    setNum(0);
  };
  let loadAllCounters = () => {
    if (counter.length === 0) {
      window.location.reload();
    }
  };
  return (
    <div className="main-container">
      <div className="cart">
        <span className="cart-img"></span>
        <span>{num}</span>
        <p>items</p>
      </div>
      <div className="reset-btn-container">
        <button className="reload" onClick={refreshCount}></button>
        <button className="reset" onClick={loadAllCounters}></button>
      </div>
      <Counter
        cartCountInc={cartCountInc}
        cartCountDec={cartCountDec}
        counter={counter}
        setCounter={setCounter}
      />
    </div>
  );
};

export default App;
