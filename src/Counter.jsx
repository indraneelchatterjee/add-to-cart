let Counter = ({ counter, setCounter, cartCountInc, cartCountDec }) => {
  let decrement = (dataObjID) => {
    let newCounterData = counter.map((counterObj) => {
      if (counterObj.id === dataObjID) {
        if (counterObj.value === 0) {
          return counterObj;
        }
        counterObj.value === 1 && cartCountDec();
        counterObj.value = counterObj.value - 1;
      }
      return counterObj;
    });

    setCounter(newCounterData);
  };
  let increment = (dataObjID) => {
    let newCounterData = counter.map((counterObj) => {
      if (counterObj.id === dataObjID) {
        counterObj.value === 0 && cartCountInc();
        counterObj.value = counterObj.value + 1;
      }
      return counterObj;
    });

    setCounter(newCounterData);
  };
  let deleteComponent = (dataObjID) => {
    let newCounterData = counter.filter((counterObj) => {
      if (counterObj.id === dataObjID && counterObj.value > 0) {
        cartCountDec();
      }
      return counterObj.id !== dataObjID;
    });

    setCounter(newCounterData);
  };
  return counter.map((counterObj) => {
    return (
      <div className="counter-main-div" key={counterObj.id}>
        <span className="counter">
          {counterObj.value === 0 ? "Zero" : counterObj.value}
        </span>
        <button className="inc-btn" onClick={() => increment(counterObj.id)}>
          +
        </button>
        <button className="dec-btn" onClick={() => decrement(counterObj.id)}>
          -
        </button>
        <button
          className="del-btn"
          onClick={() => {
            deleteComponent(counterObj.id);
          }}
        ></button>
      </div>
    );
  });
};
export default Counter;
